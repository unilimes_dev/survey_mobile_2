import React from 'react';
import {createStackNavigator, createSwitchNavigator, createAppContainer} from 'react-navigation';
import {View} from 'react-native';
import AuthLoadingScreen from './app/views/auth/AuthLoadingScreen';
import SignInScreen from './app/views/auth/SignInScreen';
import MainApp from './app/views/main';
// import AppStack from './app/views/main';
import {
    Platform,
    TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import PowerlinesListShow from './app/views/main/home/Modal.Powerlines/show';
import ForgotPswScreen from "./app/views/auth/ForgotPswScreen";
import HeaderApp from "./app/views/main/Header";

const AppStack = createStackNavigator({
    MainApp: {
        screen: MainApp,
        navigationOptions: ({navigation}) => ({
            header: (
                <HeaderApp navigation={navigation}/>
            )
        })
    }
});
const AuthStack = createStackNavigator({
    SignIn: SignInScreen,
    ForgotPsw: ForgotPswScreen
});

const AppNavigator = createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));
export default AppNavigator;



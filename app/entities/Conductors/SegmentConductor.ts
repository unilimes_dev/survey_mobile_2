import {Pole} from "../types/Pole";
import {GPSCoordinate} from "../types/GPSCoordinate";
import {MapConductor} from "./MapConductor";
import {SETTINGS} from "../utils";

declare var google: any;


export class SegmentConductor extends MapConductor {
    private points: Array<Pole> = [];

    draw(point: any) {
        this.setMapOnAll(null, this.tempElements);
        if (!point) return console.warn('Pole not fuond!!!');
        this.points.push(point);
        if (this.points.length < 1) return
        // if (this.points.length > 2) this.points.shift();
        this.tempElements = [];

        const segmentPath = new google.maps.Polyline({
            editable: this.editable,
            draggable: this.draggable,
            clickable: this.clickable,
            path: this.points,//.map((el: Pole) => el.points.toGPS()),
            geodesic: true,
            ...(this.editable ||true ? SETTINGS.ACTIVE.LINE : SETTINGS.IN_ACTIVE.LINE)
        });
        this.tempElements.push(segmentPath);
        this.setMapOnAll(this.map, this.tempElements);
    }
}

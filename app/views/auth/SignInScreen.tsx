import React from "react";
import {
    View,
    Button
} from "react-native";
import {Image} from 'react-native-elements';
import {ActivityIndicator} from 'react-native';
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState
} from "react-navigation";
import {
    Text
} from 'react-native';

import {Form, Field} from 'react-native-validate-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {signIn, changeSettings, moduleName, userSelector} from "../../ducks/auth";
import Loading from "../../components/loading";
import styles from "../../styles/main";
import {
    SecondaryButton,
    PrimaryButton,
    InputField,
    required,
    email
} from "../../components";
import images from "../../styles/images";


interface MapProps {
    user: any,
    loading: any,
    authError: any,
    changeSettings: Function,
    signIn: Function,
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface State {
    email: string,
    password: string
    errors: Array<any>
}

class SignInScreen extends React.Component<MapProps, State> {
    static navigationOptions = {
        title: 'Sign in',
    };
    private myForm: any
    state = {
        pending: false,
        email: '',
        password: '',
        errors: [],
    };

    componentDidMount(): void {
        this.props.changeSettings({});
    }

    componentWillReceiveProps(nextProps: Readonly<MapProps>, nextContext: any): void {
        if (nextProps.user !== this.props.user) {
            this.props.navigation.navigate('App');
        }
    }

    private onForgotPsw = () => {
        this.props.navigation.navigate('ForgotPsw');
    }

    private submitForm = () => {
        let submitResults = this.myForm.validate();

        let errors = [];

        submitResults.forEach(item => {
            errors.push({field: item.fieldName, error: item.error});
        });

        this.setState({errors: errors});
        if (errors.filter((el: any) => el.error).length === 0) {
            const newState: any = {pending: true};
            this.setState(newState);
            this.props.signIn(this.state);
        }
    }
    private onChange = (state) => {
        this.props.changeSettings({});
        this.setState({
            ...state,
            errors: []
        })
    }

    render() {
        const {authError} = this.props;
        const buttonTitle =   'Sign in';
        return (
            <View style={styles.mainPage}>
               <View  style={styles.logoImgContainer}>
                   <Image
                       source={images.LOGO}
                       style={styles.logoImg}
                       PlaceholderContent={<ActivityIndicator/>}
                   />
               </View>
                <View style={styles.container}>
                    <Text style={styles.title}>Welcome</Text>
                    <Form
                        style={{width: '100%'}}
                        ref={(ref) => this.myForm = ref}
                        validate={true}
                        errors={this.state.errors}
                    >

                        <Field
                            style={{width: '100%'}}
                            required
                            placeholder="Enter email"
                            component={InputField}
                            validations={[required, email]}
                            name="email"
                            value={this.state.email}
                            onChangeText={(email) => this.onChange({email})}
                        />
                        <Field
                            style={{width: '100%'}}
                            secureTextEntry={true}
                            required
                            placeholder="Enter password"
                            component={InputField}
                            validations={[required]}
                            name="password"
                            value={this.state.password}
                            onChangeText={(password) => this.onChange({password})}
                        />
                    </Form>

                    <View>
                        <SecondaryButton
                            title={'Forgot password?'}
                            onPress={this.onForgotPsw}
                        />
                    </View>
                    <PrimaryButton
                        style={{width: '100%'}}
                        title={buttonTitle}
                        disabled={this.props.loading}
                        onPress={this.submitForm}
                    />
                    {
                        authError ? (
                            <Text style={{color: 'red'}}>
                                Error! Either user or password are wrong. Please try again
                            </Text>
                        ) : null
                    }
                </View>
            </View>
        );
    }

}


const mapStateToProps = (state: any) => ({
    user: userSelector(state),
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeSettings,
        signIn,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);


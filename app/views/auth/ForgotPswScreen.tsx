import React from "react";
import {
    View,
} from "react-native";
import {Image} from 'react-native-elements';
import {ActivityIndicator} from 'react-native';
import {
    NavigationParams,
    NavigationScreenProp,
    NavigationState
} from "react-navigation";
import {
    Text
} from 'react-native';

import {Form, Field} from 'react-native-validate-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {changeSettings, reqResetPsw, moduleName, userSelector} from "../../ducks/auth";
import styles from "../../styles/main";
import {
    SecondaryButton,
    PrimaryButton,
    InputField,
    required,
    email
} from "../../components";
import images from "../../styles/images";


interface MapProps {
    reqResetPsw: any,
    refreshed: any,
    user: any,
    loading: any,
    authError: any,
    signIn: Function,
    navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface State {
    email: string,
    errors: Array<any>
}

class ForgotPswScreen extends React.Component<MapProps, State> {
    static navigationOptions = {
        title: 'Forgot Password',
    };
    private myForm: any;
    state = {
        email: '',
        errors: [],
    };

    componentDidMount(): void {
        this.props.changeSettings({});
    }

    componentWillReceiveProps(nextProps: Readonly<MapProps>, nextContext: any): void {
        if (nextProps.user !== this.props.user) {
            this.props.navigation.navigate('App');
        }
        if (nextProps.refreshed !== this.props.refreshed) {
            this.props.navigation.navigate("SignIn");
        }
        if (nextProps.refreshed !== this.props.refreshed) {
            this.props.navigation.navigate("SignIn");
        }
    }

    private onBack = () => {
        this.props.navigation.navigate('SignIn');
    }

    private submitForm = async () => {
        let submitResults = this.myForm.validate();

        let errors = [];

        submitResults.forEach(item => {
            errors.push({field: item.fieldName, error: item.error});
        });

        this.setState({errors: errors});
        if (errors.filter((el: any) => el.error).length === 0) {
            try {
                const newState: any = {pending: true};
                this.setState(newState);
                await this.props.reqResetPsw(this.state);
            } catch (e) {
            }
        }
    }


    private onChange = (state) => {
        this.props.changeSettings({});
        this.setState({
            ...state,
            errors: []
        })
    }

    render() {
        const {authError} = this.props;
        return (
            <View style={styles.mainPage}>
                <View  style={styles.logoImgContainer}>
                    <Image
                        source={images.LOGO}
                        style={styles.logoImg}
                        PlaceholderContent={<ActivityIndicator/>}
                    />
                </View>
                <View style={styles.container}>
                    <Text style={styles.title}>Reset Password</Text>
                    <Form
                        style={{width: '100%'}}
                        ref={(ref) => this.myForm = ref}
                        validate={true}
                        errors={this.state.errors}
                    >
                        <Field
                            style={{width: '100%'}}
                            required
                            placeholder="Enter email"
                            component={InputField}
                            validations={[required, email]}
                            name="email"
                            value={this.state.email}
                            onChangeText={(email) => this.onChange({email})}
                        />
                    </Form>

                    <View>
                        <SecondaryButton
                            title={'Back to Login'}
                            onPress={this.onBack}
                        />
                    </View>
                    <PrimaryButton
                        style={{width: '100%'}}
                        title={'Reset password!'}
                        disabled={this.props.loading}
                        onPress={this.submitForm}
                    />
                    {
                        authError ? (
                            <Text style={{color: 'red'}}>
                                User not found
                            </Text>
                        ) : null
                    }
                </View>
            </View>
        );
    }
}


const mapStateToProps = (state: any) => ({
    refreshed: state[moduleName].refreshed,
    user: userSelector(state),
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeSettings,
        reqResetPsw,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPswScreen);


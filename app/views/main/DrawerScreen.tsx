import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {
    ActivityIndicator,
    Dimensions, FlatList,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {DrawerActions} from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";
import {Image} from "react-native-elements";
import images from "../../styles/images";
import styles from "../../styles/main";
import ProjectListShow from './home/View.Projects/show';
import PowerlinesListShow from './home/View.Powerlines';
import Entities from './home/View.Entities';
import {COLORS} from "../../styles/colors";
import SvgUri from "react-native-svg-uri";
import {sync} from "../../sync/synch.data";
import Loading from "../../components/loading";
import Filter from "./home/View.Filter";

interface ItemProps {
    navigation: any
}

interface ItemState {
    isSync: any
}

class DrawerScreen extends Component <ItemProps, ItemState> {
    static navigationOptions = {
        header: null
    };
    state = {
        isSync: false
    }
    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
    }
    private handleClose = () => {
        // DrawerActions.closeDrawer();
        this.props.navigation.toggleDrawer();
    }
    private onSync = async () => {
        if (this.state.isSync) return;
        this.setState({isSync: true});
        await sync();
        this.setState({isSync: false});
    }

    render() {
        return (
            <View style={localStyles.container}>
                <View style={styles.header}>
                    <Image
                        source={images.LOGO}
                        style={localStyles.logoImg}
                        PlaceholderContent={<ActivityIndicator/>}
                    />
                    <TouchableOpacity onPress={this.handleClose}>
                        <Icon size={30}
                              style={{paddingRight: 20}}
                              name={Platform.OS === "ios" ? "ios-close" : "md-close"}
                        />
                    </TouchableOpacity>
                </View>
                <View style={localStyles.menuContainer}>
                    {
                        this.state.isSync ? (
                            <Loading/>
                        ) : (
                            <TouchableOpacity onPress={this.onSync} style={localStyles.menuItem}>
                                <SvgUri
                                    width={Dimensions.get('window').width * 0.2}
                                    height={28}
                                    source={images.SyncIcon}
                                />
                                <Text style={{marginTop: 9}}>Sync</Text>
                            </TouchableOpacity>
                        )
                    }

                    <View style={localStyles.menuSeparator}/>
                    <View style={localStyles.menuItem}>
                        <SvgUri
                            width={Dimensions.get('window').width * 0.2}
                            height={28}
                            source={images.MapViewIcon}
                        />
                        <Text style={{marginTop: 9}}>Map</Text>
                    </View>

                    <View style={localStyles.menuItem}>
                        <SvgUri
                            width={Dimensions.get('window').width * 0.2}
                            height={28}
                            source={images.TableViewIcon}
                        />
                        <Text style={{marginTop: 9}}>Table</Text>
                    </View>
                </View>


                <ScrollView>
                    <ProjectListShow/>
                    <PowerlinesListShow/>
                    <Entities/>
                    <Filter />
                </ScrollView>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        padding: 10,
        flex: 1,
        justifyContent: 'center',
        width: Dimensions.get("window").width
    },
    logoImg: {
        width: Dimensions.get("window").width - 60
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    menuContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 50,
        paddingBottom: 30,
        borderBottomWidth: 2,
        borderBottomColor: '#eeeeee',
        borderBottomEndRadius: 1,
    },
    menuItem: {
        // borderBottomColor: COLORS.GRAY,
        // borderBottomWidth: 1,
        // paddingBottom: 10,
        // paddingTop: 10,
        display: 'flex',
        alignItems: 'center',
    },
    menuSeparator: {
        width: 2,
        backgroundColor: '#eeeeee',
    },
});
export default DrawerScreen;

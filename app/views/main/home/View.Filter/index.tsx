import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import RadioGroup from "./ui.components/radio-group";

import moment from 'moment';

import {
    changeContols,
    moduleName,
} from "../../../../ducks/map";

import {
    View,
    Text,
    StyleSheet, TouchableOpacity,
} from 'react-native';


class Filter extends Component<any> {
    state = {
        dates: [
            {
                title: 'All',
                value: 'All'
            },
            {
                title: 'Today ',
                subtitle: moment().format('l'),
                value: moment().utc().toString()
            },
            {
                title: 'Last 7 days ',
                subtitle: moment().subtract(7, 'days').format('l'),
                value: moment().subtract(7, 'days').utc().toString()
            },
            {
                title: 'Last 30 days ',
                subtitle: moment().subtract(30, 'days').format('l'),
                value: moment().subtract(30, 'days').utc().toString()
            }
        ]
    };

    private handleChange = (name, value) => {
        this.props.changeContols({name, value});
    };

    render() {
        const {dates} = this.state;
        const {dateFilter} = this.props;
        return (
            <View style={localStyles.container}>
                <Text style={localStyles.title}>Record Updated:</Text>
                <View style={localStyles.radioGroup}>
                    {
                        dates.map((el: any) => {
                            return (
                                <TouchableOpacity key={el.value}
                                    onPress={() => this.handleChange('dateFilter', el.value)}>
                                    {
                                        dateFilter === el.value ? (
                                            <View style={localStyles.radioBtn}>
                                                <View style={localStyles.checkedCircle}>
                                                    <View style={localStyles.checkedInnerCircle}/>
                                                </View>
                                                <View style={localStyles.containerText}>
                                                    <Text style={localStyles.checkedText}>{el.title}</Text>
                                                    <Text style={localStyles.checkedText}>{el.subtitle}</Text>
                                                </View>
                                            </View>
                                        ) : (
                                            <View style={localStyles.radioBtn}>
                                                <View style={localStyles.circle} />
                                                <View style={localStyles.containerText}>
                                                    <Text>{el.title}</Text>
                                                    <Text>{el.subtitle}</Text>
                                                </View>
                                            </View>
                                        )
                                    }
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        paddingTop: 30,
        paddingBottom: 5,
    },
    title: {
      marginBottom: 20
    },
    radioGroup: {},
    radioBtn: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: 15,
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        backgroundColor: '#dddee1',
        marginRight: 15,
    },
    checkedCircle: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        backgroundColor: '#315581',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
    },
    checkedInnerCircle: {
        width: 11,
        height: 11,
        borderRadius: 5.5,
        backgroundColor: '#fff',
    },
    containerText: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    checkedText: {
        fontWeight: 'bold',
    }
});

const mapStateToProps = (state: any) => {
    return {
        dateFilter: state[moduleName].dateFilter,
    }
};

const mapDispatchToProps = (dispatch: any) => {
   return bindActionCreators({
        changeContols
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);

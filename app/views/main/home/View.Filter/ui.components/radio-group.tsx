import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from "react-native";

interface ItemProps {
    title: string,
    value: string
}


class RadioGroup extends Component <any> {
    state = { value: null };

    private handleChange = (e) => {
        this.setState({value: e.target.value});
        this.props.onChangeControls({name, value: e.target.value})
    };

    render() {
        const { options } = this.props;
        return (
            <View>
                {
                    options.map(option => {
                        return (
                            <View key={option.key} style={localStyles.container}>
                                <TouchableOpacity
                                    style={localStyles.circle}
                                    onPress={() => this.handleChange()}>
                                    {
                                        this.state.value === option.title && (
                                            <View style={localStyles.checked}>
                                                <View style={localStyles.checkedInner}/>
                                            </View>
                                        )
                                    }
                                </TouchableOpacity>
                                {
                                    this.state.value === option.title ? <Text style={localStyles.checkedText}>{option.title}</Text> : <Text>{option.title}</Text>
                                }
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

const localStyles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent:'flex-start',
        alignItems: 'center',
        marginBottom: 15,
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        backgroundColor: '#dddee1',
        marginRight: 15,
    },
    checked: {
        width: 25,
        height: 25,
        borderRadius: 12.5,
        backgroundColor: '#315581',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedText: {
        fontWeight: 'bold',
    },
    checkedInner: {
        width: 11,
        height: 11,
        borderRadius: 5.5,
        backgroundColor: '#fff',
    }
});

export default RadioGroup;
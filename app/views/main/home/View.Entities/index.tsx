import React, {Component} from 'react';
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {Parcel, Poi, Pole, Project, Segment, Station} from "../../../../entities";
import {
    changeContols, locationParcelsSelector, locationPoisSelector,
    locationPolesSelector, locationSegmentsSelector,
    locationSelector, locationStationsSelector,
    moduleName
} from "../../../../ducks/map";
import styles, {list_styles} from "../../../../styles/main";
import CheckBox from '../../../../components/CheckBox'
import SvgUri from 'react-native-svg-uri';
import images from "../../../../styles/images";
import {Image} from "react-native-elements";

interface MapProps {
    project: Project,
    showDialogContent: Function,
    changeContols: Function,
    pois: Array<Poi>,
    projects: Array<Project>,
    segments: Array<Segment>,
    stations: Array<Station>,
    parcels: Array<Parcel>,
    poles: Array<Pole>
}

class Entities extends Component<MapProps> {

    private selectItem = (name: string) => {
        this.props.changeContols({name, value: !this.props[name]})
    }

    render() {
        const els: any = [
            {
                name: 'showPoles',
                title: ({styles}) => (
                    <View style={localStyles.itemInfo}>
                        <Text>Electrical pole</Text>
                        <Text>({this.props.poles.length})</Text>
                        <Image
                            style={styles.icon}
                            source={images.PoleIcon}
                        />
                    </View>
                )
            },
            {
                name: 'showSegments',
                title: ({styles}) => (
                    <View style={localStyles.itemInfo}>
                        <Text>Powerline segment</Text>
                        <Text>({this.props.segments.length})</Text>
                        <SvgUri
                            width="20"
                            height="20"
                            source={images.SegmentIcon}
                        />
                    </View>
                )
            },
            {
                name: 'showStations',
                title: ({styles}) => (
                    <View style={localStyles.itemInfo}>
                        <Text>Stations</Text>
                        <Text>({this.props.stations.length})</Text>
                        <Image
                            style={styles.icon}
                            source={images.StationIcon}
                        />
                    </View>
                )
            },
            {
                name: 'showParcels',
                title: ({styles}) => (
                    <View style={localStyles.itemInfo}>
                        <Text>Land parcel</Text>
                        <Text>({this.props.parcels.length})</Text>
                        <SvgUri
                            width="10"
                            height="10"
                            source={images.ParcelIcon}
                        />
                    </View>
                )
            },
            {
                name: 'showPois',
                title: ({styles}) => (
                    <View style={localStyles.itemInfo}>
                        <Text>POI </Text>
                        <Text>({this.props.pois.length})</Text>
                        <Image
                            style={styles.icon}
                            source={images.PoiIcon}
                        />
                    </View>
                )
            },
        ];
        return (
            <View style={styles.menuItem}>
                <View style={styles.container}>
                    <Text style={styles.filterTitle}>
                        Select Entities:
                    </Text>
                </View>
                <View style={localStyles.container}>
                    {
                        els.map((el: any) => {
                            const selected = this.props[el.name];
                            let styleItem = selected ? [list_styles.itemSelected] : [list_styles.item];
                            return (
                                <CheckBox
                                    key={el.name}
                                    onPress={() => this.selectItem(el.name)}
                                    selected={selected}
                                    text={<View style={localStyles.container}>{el.title({styles: styleItem})}</View>}
                                />
                            )
                        })
                    }
                </View>
            </View>
        );
    }
}


const localStyles = StyleSheet.create({
    icon: {
        width: 10,
        height: 10
    },
    container: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    itemInfo: {
        width: '100%',
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row'
    }
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        changeContols,
    }, dispatch)
);
const mapStateToProps = (state: any) => ({
    pois: locationPoisSelector(state),
    parcels: locationParcelsSelector(state),
    stations: locationStationsSelector(state),

    project: locationSelector(state),
    poles: locationPolesSelector(state),
    segments: locationPolesSelector(state),
    showPois: state[moduleName].showPois,
    showStations: state[moduleName].showStations,
    showSegments: state[moduleName].showSegments,
    showParcels: state[moduleName].showParcels,
    showPoles: state[moduleName].showPoles,
});
export default connect(mapStateToProps, mapDispatchToProps)(Entities);


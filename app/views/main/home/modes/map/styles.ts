import {Dimensions, StyleSheet} from "react-native";
import {COLORS} from "../../../../../styles/colors";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    addPoiBtn: {
        position: 'absolute',
        bottom: 20,
        right: 20
    },
    allowAddPoi: {
        width: Dimensions.get('window').width,
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        padding: 20,
        backgroundColor: 'white',
        textAlign: 'center',
        opacity: 0.9
    },
    allowAddPoiText: {
        color: COLORS.PRIMARY_1,
        opacity: 0.7
    },
    alertWarn: {
        display:'flex',
        margin: 0,
    },
});

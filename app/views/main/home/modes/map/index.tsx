import React from "react";
import {Dimensions, StyleSheet, Text, View} from "react-native";
import MapView from 'react-native-map-clustering';
// import Toast, {DURATION} from 'react-native-easy-toast'
import {Marker, Polygon, Polyline} from 'react-native-maps';
import {Constants} from 'expo';
import {styles} from './styles';
// import Reactotron from 'reactotron-react-native'

import {connect} from 'react-redux';
import {
    lastGeoPostionsSelector, locationParcelsSelector,
    locationPoisSelector, locationPolesSelector, locationSegmentsSelector, locationSelector,
    locationStationsSelector,
    moduleName
} from "../../../../../ducks/map";
import {Geometry, GPSCoordinate, Parcel, Poi, Pole, Project, Segment, Station} from "../../../../../entities";
import {searchSelector} from "../../../../../ducks/auth";
import {segment_statuses, statuses} from "../../../../../utils";
import {bindActionCreators} from "redux";
import {showDialogContent} from "../../../../../ducks/dialogs";
import AddParcelDialog from "../modals/add.parcel";
import AddPoleDialog from "../modals/add.pole";
import AddSegmentDialog from "../modals/add.segment";
import AddStationDialog from "../modals/add.station";
import AddPoiDialog from "../modals/add.poi";
import images from '../../../../../styles/images';
import {FabButton} from "../../../../../components/Buttons/Fab";
import {COLORS} from "../../../../../styles/colors";
import {PrimaryButton} from "../../../../../components/Buttons/Primary";

// import { YandexMapKit, YandexMapView } from 'react-native-yandexmapkit';
// YandexMapKit.setApiKey('25123855-c922-4db2-81c1-1ef498e6086b');

interface MapProps {
    project: Project,
    stations: Array<Station>,
    poles: Array<Pole>,
    segments: Array<Segment>,
    parcels: Array<Parcel>,
    pois: Array<Poi>,
    mapCenter: GPSCoordinate,
    showDialogContent: Function,
    search: string,
}

interface MapState {
    allowAddPoi: boolean
}

/*
* NOT able to split on smaller components
* */
class MapViewMode extends React.Component<MapProps, MapState> {

    private map: any;
    private searchCount: number = 0;
    private searchAvailabelCount: number = 0;

    state = {
        allowAddPoi: false
    };

    private filterItems(list: Array<any>, search: string) {
        if (!search) return list;
        let _list = [];
        const keys = list.length ? list[0].keys() : [];
        for (let i = 0; i < list.length; i++) {
            const el: any = list[i];
            if (search) {
                let isInseach = false;
                for (let j = 0; j < keys.length; j++) {
                    const val = el[keys[j]];
                    if (val && val.toString().toLowerCase().match(search.toLowerCase())) {
                        isInseach = true;
                        break;
                    }
                }
                if (!isInseach) continue;
            }
            _list.push(el);
        }
        return _list;
    }

    private showDialog = (item) => {
        const {showDialogContent} = this.props;

        if (item instanceof Parcel) {
            showDialogContent(
                {
                    content: (
                        <AddParcelDialog
                            selectedItem={item}
                        />
                    ),
                    header: (
                        <Text>Edit Parcel({item.id})</Text>
                    )
                }
            );
        } else if (item instanceof Pole) {
            showDialogContent(
                {
                    content: (
                        <AddPoleDialog
                            selectedItem={item}
                        />
                    ),
                    header: (
                        <Text>Edit Pole({item.id})</Text>
                    )
                }
            );
        } else if (item instanceof Segment) {
            showDialogContent(
                {
                    content: (
                        <AddSegmentDialog
                            selectedItem={item}
                        />
                    ),
                    header: (
                        <Text>Edit Segment({item.id})</Text>
                    )
                }
            );
        } else if (item instanceof Station) {

            showDialogContent(
                {
                    content: (
                        <AddStationDialog
                            selectedItem={item}
                        />
                    ),
                    header: (
                        <Text>Edit Poi({item.id})</Text>
                    )
                }
            );
        } else if (item instanceof Poi) {
            showDialogContent(
                {
                    content: (
                        <AddPoiDialog
                            selectedItem={item}
                        />
                    ),
                    header: (
                        <Text>Edit Poi({item.id})</Text>
                    )
                }
            );
        }
    }

    private addPoi = () => {
        this.setState({allowAddPoi: true});
    };
    private onMapClick = (e: any) => {
        const {project, showDialogContent} = this.props;

        if (!project) {
            return showDialogContent(
                {
                    content: (
                        <Text style={styles.alertWarn}>Please select Project first</Text>
                    ),
                    header: (
                        <Text>Warning</Text>
                    )
                }
            );
        }
        this.drawInMap(e.nativeEvent.coordinate);
    }

    private onPress = () => {

    }

    private drawInMap(event: any) {
        const {showDialogContent} = this.props;

        const coordinate = [
            event.latitude,
            event.longitude
        ];
        showDialogContent(
            {
                content: (
                    <AddPoiDialog
                        selectedItem={new Poi({projectId: this.props.project ? this.props.project.id : -1})}
                        position={new Geometry(Geometry.TYPE.POINT, coordinate)}/>
                ),
                header: (
                    <Text>Add poi</Text>
                )/*,
                buttons: (
                    <PrimaryButton
                        onPress={this.onPress}
                    />
                )*/
            }
        );
    }

    render() {

        const {props} = this;
        this.searchCount = 0;
        this.searchAvailabelCount = 0;
        [
            'showPois',
            'showParcels',
            'showSegments',
            'showStations',
            'showPois',
        ].forEach((el) => {
            if (props[el]) {
                this.searchAvailabelCount++;
            }
        });


        const {
            mapCenter,
            stations,
            pois,
            parcels,
            segments,
            poles,
            search,
        } = this.props;

        const _poles = this.filterItems(poles, search);
        const _segments = this.filterItems(segments, search);
        const _pois = this.filterItems(pois, search);
        const _stations = this.filterItems(stations, search);
        const _parcels = this.filterItems(parcels, search);
        return (
            <View style={styles.container}>
                <MapView
                    onPress={this.onMapClick}
                    ref={ref => {
                        this.map = ref;
                    }}
                    region={{
                        ...mapCenter,
                        latitudeDelta: 8.5,
                        longitudeDelta: 8.5
                    }}
                    style={styles.container}
                >

                    {
                        _stations.map((marker: Station) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={images.StationIcon}
                                onPress={() => this.showDialog(marker)}
                            />
                        ))
                    }
                    {
                        _poles.map((marker: Pole) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={images.PoleIcon}
                                onPress={() => this.showDialog(marker)}
                            />
                        ))
                    }
                    {
                        _pois.map((marker: Poi) => (
                            <Marker
                                key={marker.id}
                                coordinate={marker.points.toGPS()}
                                image={images.PoiIcon}
                                onPress={() => this.showDialog(marker)}
                            />
                        ))
                    }
                    {
                        _parcels.map((marker: Parcel) => {
                            let strokeColor = '#000';
                            switch (marker.status) {
                                case segment_statuses[0].value: {
                                    strokeColor = 'blue';
                                    break;
                                }
                                case segment_statuses[1].value: {
                                    strokeColor = 'yellow';
                                    break;
                                }
                                case segment_statuses[2].value: {
                                    strokeColor = 'orange';
                                    break;
                                }
                                case segment_statuses[3].value: {
                                    strokeColor = 'red';
                                    break;
                                }
                                case segment_statuses[4].value: {
                                    strokeColor = 'green';
                                    break;
                                }
                                case segment_statuses[5].value: {
                                    strokeColor = 'grey';
                                    break;
                                }
                                case segment_statuses[6].value: {
                                    strokeColor = 'magenta';
                                    break;
                                }
                            }
                            return (
                                <Polygon
                                    key={marker.id}
                                    coordinates={marker.pathList}
                                    strokeWidth={2}
                                    strokeColor={strokeColor}
                                    onPress={() => this.showDialog(marker)}
                                />
                            )
                        })
                    }
                    {
                        _segments.map((marker: Segment) => {
                            let strokeColor = marker.status === statuses[0].value ? 'blue' : (marker.status === statuses[1].value ? 'green' : 'red');

                            return (
                                <Polyline
                                    key={marker.id}
                                    coordinates={marker.pathList}
                                    strokeWidth={2}
                                    strokeColor={strokeColor}
                                    onPress={() => this.showDialog(marker)}
                                />
                            )
                        })
                    }

                </MapView>
                <FabButton
                    style={styles.addPoiBtn}
                    onPress={this.addPoi}
                />
                {
                    this.state.allowAddPoi ? (
                        <View style={styles.allowAddPoi}>
                            <Text style={styles.allowAddPoiText}>Click on the map to set the location</Text>
                        </View>
                    ) : null
                }
            </View>

        );
    }
}

const mapStateToProps = (state: any) => ({
    mapCenter: state[moduleName].mapCenter,
    stations: locationStationsSelector(state),


    project: locationSelector(state),
    // selected_powerlines: powerlineSelector(state),
    search: searchSelector(state),
    mapZoom: state[moduleName].mapZoom,
    dateFilter: state[moduleName].dateFilter,
    allowAddPoi: state[moduleName].allowAddPoi,
    poiList: state[moduleName].poiList,
    segmentList: state[moduleName].segmentList,
    stationList: state[moduleName].stationList,
    polesList: state[moduleName].polesList,
    parcelList: state[moduleName].parcelList,
    showStations: state[moduleName].showStations,
    showSegments: state[moduleName].showSegments,
    showParcels: state[moduleName].showParcels,
    showPoles: state[moduleName].showPoles,
    showPois: state[moduleName].showPois,
    pois: locationPoisSelector(state),
    tempPosition: lastGeoPostionsSelector(state),
    segments: locationSegmentsSelector(state),
    poles: locationPolesSelector(state),
    parcels: locationParcelsSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(MapViewMode);


import React, {Component} from 'react';

import {Form, Field} from 'react-native-validate-form';
import {View, Text, Platform, TouchableOpacity, StyleSheet, TextInput, Slider} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import NumericInput from 'react-native-numeric-input';
// import SelectInput from '@tele2/react-native-select-input';
// import UploadFile from '../../../../../components/upload';
import {
    Upload,
    Pole,
    Parcel, Segment, Station, Category
} from "../../../../../../entities";
import {
    statuses,
    segment_statuses,
    segment_operation_type,
    required,
} from "../../../../../../utils";
import {InputField} from "../../../../../../components/Input/InputField";
import { Dropdown } from 'react-native-material-dropdown';
import {checkError} from "../../../../../../utils";

import {PrimaryButton} from "../../../../../../components/Buttons/Primary";
import MultiSelect from "react-native-multiple-select";
import {SecondaryButton} from "../../../../../../components/Buttons/Secondary";

interface MapProps {
    isAdmin: any,
    itemsList: any,
    position: any,
    selectedItem: any,
    location: any,
    categories: Array<Category>,
    projects: Array<any>,
    tempPosition: Array<any>,
    onFinishEditItem: Function,
    changeContols: Function,
    editItem: Function,
    onDeleteItem: Function,
    onAddItem: Function,
    setDialogSaveButton: Function,
    showDialogContent: Function
}

interface MapState {
    uploads: Array<Upload>,
    errors: any,
    canDelete: boolean,
    __pending: boolean
}

export const TYPES = {
    NONE: -1,
    PARCEL: 1,
    POLE: 2,
    STATION: 3,
    SEGMENT: 4,
    POI: 5,
};

export default class MainModalDialog extends Component<MapProps, MapState> {

    protected editTitle: boolean = true;
    protected title: string = '';
    protected type: number = TYPES.NONE;
    private myForm: any;
    static defaultProps: {
        categories: [],
        projects: [],
        itemsList: null,
        position: null,
        tempPosition: [],
        onAddItem: () => false,
        onDeleteItem: () => false,
        onFinishEditItem: () => false,
        changeContols: () => false
    };

    constructor(p: any) {
        super(p);
        this.state = {
            __pending: false,
            canDelete: false,
            errors: [],
            ...p.selectedItem
        }
    }

    componentDidMount(): void {
        this.props.setDialogSaveButton(
            (
                <PrimaryButton
                    style={{width: 70, marginRight: 10}}
                    title={'Save'}
                    onPres={this.handleOk}
                />
            )
        )
    }

    componentWillReceiveProps(nextProps: any, nextContext: any): void {
        checkError(nextProps, this.props, () => 1, this.refs.toast);
        if (nextProps.itemsList !== this.props.itemsList) {
            this.setState({__pending: false});
            this.handleCancel({});
        }
    }

    private onUploadFile = (fileList: any) => {
        this.setState({
            uploads: [
                ...this.state.uploads,
                ...fileList
            ]
        })
    };
    private onUpdateFile = (fileList: any) => {
        this.setState({
            uploads: [
                ...fileList
            ]
        })
    };

    private onFieldChange = (key: string) => {
      return (val: any) => {
          const newState: any ={
              [key]: val
          };
          this.setState(newState);
      }
    };

    private onChange = (e: any) => {
        let value = e.target.value;
        if(e.target.getAttribute instanceof Function && e.target.getAttribute('type') === 'number') {
            value = parseFloat(value);
            const min = parseInt(e.target.getAttribute('min'));
            const max = parseInt(e.target.getAttribute('max'));
            if(!isNaN(max) && value > max) {
                value = max;
            }
            if(!isNaN(min) && value < min) {
                value = min;
            }
        }

        const newState: any = {
            [e.target.name]: value
        };

        this.setState(newState);
    };


    protected handleOk = async (e: any) => {
        try {
            this.setState({__pending: true});
            await this.props.editItem({
                ...this.state,
            });
            this.props.onFinishEditItem();

        } catch (e) {
            // toast.show(e.response ? e.response.data.error || e.response.data.message : e.meesage || e, {
            //     position: toast.POSITION.TOP_LEFT
            // });
        } finally {
        }
    };

    protected handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };

    protected deleteItem = async (e: any) => {
        try {
            this.props.onDeleteItem({
                ...this.state,
            });
            this.props.onFinishEditItem();

        } catch (e) {

        }
        this.handleCancel(e);
        return false
    };

    private submitForm = () => {
        let submitResults = this.myForm.validate();

        let errors = [];

        submitResults.forEach(item => {
            errors.push({field: item.fieldName, error: item.error});
        });

        this.setState({errors: errors});
    };

    private getFields = () => {
        const {selectedItem}: any = this.props;
        const fields = [];
        const {state} = this;
        if (this.type === TYPES.PARCEL) {
            fields.push(
                {
                    title: 'Status',
                    name: 'status',
                    options: statuses
                },
                ...Parcel.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.POLE) {
            fields.push(
                ...Pole.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.SEGMENT) {
            fields.push(
                {
                    title: 'Status',
                    name: 'status',
                    options: segment_statuses
                },
                {
                    options: [0, 25, 50, 75, 100].map((el: number) => ({
                        value: el,
                        text: el,
                    })),
                    name: 'vegetation_status',
                    title: 'Vegetation status'
                },
                {
                    type: 6,
                    step: 1,
                    min: 0,
                    max: 10,
                    name: 'distance_lateral',
                    title: 'Distance lateral'
                },
                {
                    type: 6,
                    step: 1,
                    min: 0,
                    max: 15,
                    name: 'distance_bottom',
                    title: 'Distance bottom'
                },
                ...Segment.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
            if(state.status === segment_statuses[3].value) {
                fields.push(
                    {
                        type: 6,
                        step: 1,
                        min: 1,
                        max: 12,
                        name: 'shutdown_time',
                        title: 'Shutdown time'
                    },
                    {
                        name: 'track',
                        title: 'Track',
                        options: [1, 2].map((el:number) => ({
                            value: el,
                            text: el
                        }))
                    }
                )
            }
            if([
                segment_statuses[1].value,
                segment_statuses[2].value,
                segment_statuses[3].value,
                segment_statuses[4].value,
                segment_statuses[6].value,
            ].indexOf(state.status) > -1) {
                fields.push(
                    {
                        type: 3,
                        name: 'operation_type',
                        title: 'Operation type',
                        options: segment_operation_type
                    },
                    {
                        type: 6,
                        step: 1,
                        min: 1,
                        max: 12,
                        name: 'time_of_operation',
                        title: 'time of operation'
                    }
                );
            }
            if([
                segment_statuses[4].value
            ].indexOf(state.status) > -1) {
                fields.push(
                    {
                        name: 'time_for_next_entry',
                        title: 'time for next entry'
                    }
                );
            }
            if([
                segment_statuses[6].value
            ].indexOf(state.status) > -1) {
                fields.push(
                    {
                        name: 'parcel_number_for_permit',
                        title: 'parcel number for permit'
                    }
                );
            }
            fields.push(
                {
                    type: 5,
                    name: 'notes',
                    title: 'Notes'
                }
            );
        } else if (this.type === TYPES.STATION) {
            fields.push(
                ...Station.edit_keys.map((el: string) => ({
                    title: el,
                    name: el
                }))
            );
        } else if (this.type === TYPES.POI) {
            fields.push(
                {
                    title: 'Project',
                    name: 'ProjectId',
                    options: this.props.projects.map((el: any) => ({
                        text: el.title,
                        value: el.id
                    }))
                },
                {
                    title: 'Category',
                    name: 'categoryId',
                    required: true,
                    options: this.props.categories.map((el: any) => ({
                        text: el.title,
                        value: el.id
                    }))
                }
            )
        }
        return fields;
    };

    protected _render() {
        const state: any = this.state;
        const {title, comment}: any = this.state;
        const {selectedItem}: any = this.props;
        const fields = this.getFields();

        const {isAdmin} = this.props;

        return (

            <View>
                <View style={localStyles.modalTitle}>
                    <Text>{selectedItem.id ? 'Edit' : 'Create'} {this.title} {selectedItem.id ? `(${selectedItem.id})` : ''}</Text>
                    <TouchableOpacity onPress={this.handleCancel}>
                        <Icon size={30}
                              style={{paddingRight: 0}}
                              name={Platform.OS === "ios" ? "ios-close" : "md-close"}
                        />
                    </TouchableOpacity>
                </View>

                <Form
                    style={{minWidth: '100%'}}
                    ref={(ref) => this.myForm = ref}
                    validate={true}
                    errors={this.state.errors}>
                    {
                        fields.map((el: any) => {
                            if (el.type === 2) {
                                return (
                                    <View key={el.name}>
                                        <Text>{el.title}</Text>
                                        <Slider
                                            onValueChange={(value) => this.onChange({target: {name: el.name, value}})}
                                            value={state[el.name]}
                                            step={el.step}
                                            minimumValue={el.min}
                                            maximumValue={el.max}
                                        />
                                    </View>
                                )
                            } else if (el.type === 3) {
                                return (
                                    <View key={el.name}>
                                        <Text>{el.title}</Text>
                                        <MultiSelect
                                            items={el.options.map((opt:any) => {
                                                opt.text
                                            })}
                                            onSelectedItemsChange={this.onChange}
                                            hideTags
                                            searchInputPlaceholderText='Search...'
                                        />
                                    </View>
                                )
                            } else if (el.type === 4) {
                                return (
                                    <View key={el.name}>
                                        <Text>{el.title}</Text>

                                    </View>
                                )
                            } else if (el.type === 5) {
                                return (
                                    <Field
                                        required
                                        component = {InputField}
                                        placeholder = {el.title}
                                        validations ={[required]}
                                        name ={el.name}
                                        multiline = {true}
                                        numberOfLines = {5}
                                        value = {el.value}
                                        onChangeText = {this.onFieldChange(el.name)}
                                    />
                                )
                            } else if (el.type === 6) {
                                return (
                                    <NumericInput
                                        //label={el.title}
                                        //name={el.name}
                                        value={state[el.name]}
                                        minValue={el.min}
                                        maxValue={el.max}
                                        onChange={this.onChange}
                                        //onLimitReached={}
                                        totalWidth={'100%'}
                                        //totalHeight={}
                                        iconSize={15}
                                        step={0.01}
                                        valueType='real'
                                        rounded
                                    />
                                )
                            }
                            if (el.options) {
                                return (
                                    <Field
                                        key={el.name}
                                        onChanheText={this.onFieldChange(el.name)}
                                        label={el.name}
                                        placeholder={el.name}
                                        value={state[el.name]}
                                        data={el.options.map((el: any) => ({
                                            label: el.text,
                                            value: el.value
                                        }))}
                                        component={Dropdown}
                                    />
                                )
                            } else {
                                return (
                                    <Field
                                        key={el.name}
                                        required
                                        placeholder={`Enter ${el.name}`}
                                        component={InputField}
                                        validations={[required]}
                                        name={el.name}
                                        value={state[el.name]}
                                        disabled={!isAdmin}
                                        onChangeText={this.onFieldChange(el.name)}
                                        customStyle={{width: '100%'}}
                                    />
                                )
                            }
                        })
                    }

                    {
                        this.editTitle ? (
                            <View>
                                <Field
                                    required
                                    component = {InputField}
                                    placeholder = "Enter title"
                                    validations = {[]}
                                    name = {'title'}
                                    value = {title}
                                    onChangeText={this.onFieldChange('title')}
                                    customStyle = {{width: '100%'}}
                                />
                                <Field
                                    required
                                    component = {InputField}
                                    placeholder = "Enter Comment"
                                    validations ={[required]}
                                    name ={ 'comment'}
                                    multiline = {true}
                                    numberOfLines = {5}
                                    value = {comment}
                                    onChangeText = {this.onFieldChange('comment')}
                                    customStyle = {{width: '100%', height: 150}}
                                />
                            </View>
                        ) : null
                    }

                    <View style={localStyles.modalActions}>
                        <SecondaryButton
                            style={{width: '30%'}}
                            title={'Cancel'}
                            onPress={this.handleCancel}
                        />
                        <PrimaryButton
                            style={{width: '30%'}}
                            title={'Save'}
                            onPress={this.handleOk}
                        />
                        {
                            this.state.canDelete && selectedItem.id && (
                                <PrimaryButton
                                    style={{width: '30%'}}
                                    title={'Delete'}
                                    onPress={this.deleteItem}
                                />
                            )
                        }
                    </View>
                </Form>
            </View>
        )
        /*return (
            <View style={styles.container}>

                <View style={styles.scrollContent}>
                    <Form
                        ref={(ref) => this.myForm = ref}
                        validate={true}
                        submit={this.submitForm.bind(this)}
                        errors={this.state.errors}
                    >
                        {
                            fields.map((el: any) => {
                                if (el.options) {
                                    return (
                                        <SelectInput
                                            key={el.name}
                                            onChange={this.onChange(el.name)}
                                            style={{width: '100%'}}
                                            label={el.name}
                                            placeholder={el.name}
                                            value={state[el.name]}
                                            options={el.options.map((el: any) => ({
                                                label: el.text,
                                                value: el.value
                                            }))}
                                        />
                                    )
                                } else {
                                    return (
                                        <Field
                                            key={el.name}
                                            required
                                            component={InputField}
                                            validations={[required]}
                                            name={el.name}
                                            value={state[el.name]}
                                            disabled={!isAdmin}
                                            onChangeText={this.onChange(el.name)}
                                            customStyle={{width: '100%'}}
                                        />
                                    )
                                }
                            })
                        }

                        <Field
                            required
                            component={InputField}
                            validations={[]}
                            name={'title'}
                            value={title}
                            onChangeText={this.onChange('title')}
                            customStyle={{width: '100%'}}
                        />
                        <TextIField
                            multiline={true}
                            numberOfLines={4}
                            onChange={this.onChange('comment')}
                            value={comment}
                            name={'comment'}
                        />

                        {/!* {
                            selectedItem.id && (
                                <UploadFile
                                    files={selectedItem.uploads}
                                    onUpload={this.onUploadFile}
                                    onUpdateFile={this.onUpdateFile}
                                />
                            )
                        }*!/}
                    </Form>
                    );
                </View>
                <Toast
                    ref="toast"
                    style={{backgroundColor: 'bottom'}}
                    position='top'
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{color: 'white'}}
                />
            </View>
        );*/
    }
}

const localStyles = StyleSheet.create({
    formContainer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 0,
    },
    modalTitle: {
        display: 'flex',
        flex: 0.9,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 0,
    },
    modalActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    }
});


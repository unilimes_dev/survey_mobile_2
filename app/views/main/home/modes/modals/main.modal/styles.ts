import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        width: '100%',
        minHeight: 120
    },
    scrollContent: {
        flex: 1,
        paddingTop: 5,
        width: '100%',
        minHeight: 120
    },
    modalTitle: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-around'
    },
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
});
export default styles

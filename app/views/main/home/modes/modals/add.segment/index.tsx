import React, {Component} from 'react';
import {
    errorSelector,
    lastGeoPostionsSelector,
    locationSelector,
    changeContols,
    moduleName
} from "../../../../../../ducks/map";
import {addSegments, editSegments} from "../../../../../../ducks/map/segments";
import {showDialogContent,setDialogSaveButton} from "../../../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import {isSuperADMINAdminSelector, isSuperAdminSelector} from "../../../../../../ducks/auth";

import MainModalDialog, {TYPES} from "../main.modal";

class AddSegmentDialog extends MainModalDialog {

    constructor(p: any) {
        super(p);
        this.title = 'Segment';
        this.type = TYPES.SEGMENT;
    }

    render() {
        return super._render();
    }
}

const mapStateToProps = (state: any) => ({
    itemsList: state[moduleName].segmentList,
    error: errorSelector(state),
    location: locationSelector(state),
    isAdmin: isSuperADMINAdminSelector(state),
    tempPosition: lastGeoPostionsSelector(state)
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        setDialogSaveButton,
        showDialogContent,
        editItem: editSegments,
        addItem: addSegments,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddSegmentDialog);
export default edit;

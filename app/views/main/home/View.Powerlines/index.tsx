import React, {Component} from 'react';
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    Dimensions, FlatList,
    Platform, ScrollView, StyleSheet, Text,
    TouchableOpacity, View
} from 'react-native';
import {Powerline, Project} from "../../../../entities";
import {
    locationSelector,
    powerlineSelector,
    powerlinesSelector,
    changeContols,
} from "../../../../ducks/map";
import {
    fetchLocationPoles
} from "../../../../ducks/map/poles";

import {
    fetchLocationParcels
} from "../../../../ducks/map/parcels";

import CheckBox from '../../../../components/CheckBox'

interface MapProps {
    changeContols: Function,
    fetchLocationParcels: Function,
    fetchLocationPoles: Function,
    showDialogContent: Function,
    project: Project,
    powerlines: Array<Powerline>,
    selected_powerlines: Array<number>
}

class PowerlinesListShow extends Component<MapProps> {

    state = {
        isAll: false
    };
    private loadItemData = (item: any) => {
        const reqData = {...this.props.project, powerLineId: item.id};
        this.props.fetchLocationParcels(reqData);
        this.props.fetchLocationPoles(reqData);
    }
    private selectItem = (item: any) => {
        let list: Array<number> = this.props.selected_powerlines;
        if (!item) {
            list = this.state.isAll ? [] : [...this.props.powerlines.map(el => el.id)];
            this.props.changeContols({
                name: 'selected_powerlines',
                value: [...list]
            });
            this.setState({
                isAll: !this.state.isAll
            });
            return this.props.powerlines.forEach((el: any) => {
                this.loadItemData(el);
            });
        }

        for (let i = 0; i < list.length; i++) {
            if (list[i] === item.id) {
                list.splice(i, 1);
                this.props.changeContols({
                    name: 'selected_powerlines',
                    value: [...list]
                });
                return;
            }
        }

        list.push(item.id);
        this.props.changeContols({
            name: 'selected_powerlines',
            value: [...list]
        });
        list.push(item.id);
        this.loadItemData(item);
    }
    private renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#CED0CE",
                }}
            />
        );
    };

    render() {
        const styles: any = {paddingRight: 20};
        if (!this.props.project) {
            return null;
        }
        return (
            <View style={styles.menuItem}>
                <View>
                    <Text style={styles.filterTitle}>
                        Select Powerline:
                    </Text>
                </View>
                <View style={localStyles.container} key={"powerlines"}>
                    <ScrollView style={styles.scrollContent}>
                        <FlatList style={styles.scrollContent}
                                  ItemSeparatorComponent={this.renderSeparator}
                                  data={
                                      [
                                          {
                                              key: 'All',
                                              title: 'All'
                                          },
                                          ...this.props.powerlines
                                      ]
                                  }
                                  renderItem={
                                      ({item, index, separators}: any) => {
                                          const selected = item.id ? (this.props.selected_powerlines.indexOf(item.id) > -1) : this.state.isAll;
                                          let styleItem = selected ? [styles.itemSelected] : [styles.item];
                                          return (
                                              <CheckBox
                                                  onPress={() => this.selectItem(item.id ? item : null)}
                                                  selected={selected}
                                                  text={<Text style={styleItem}>{item.title}</Text>}
                                              />
                                          )
                                      }
                                  }
                        />
                    </ScrollView>
                </View>
            </View>
        );
    }
}


const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        width: '100%',
        maxHeight: 120
    },
    scrollContent: {
        flex: 1,
        paddingTop: 5,
        width: '100%',
        maxHeight: 120
    },
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        fetchLocationParcels,
        fetchLocationPoles,
        showDialogContent,
    }, dispatch)
);

const mapStateToProps = (state: any) => ({
    project: locationSelector(state),
    powerlines: powerlinesSelector(state),
    selected_powerlines: powerlineSelector(state),
});
export default connect(mapStateToProps, mapDispatchToProps)(PowerlinesListShow);


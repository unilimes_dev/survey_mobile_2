import React, {Component} from 'react';
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    Dimensions,
    Platform, StyleSheet, Text,
    TouchableOpacity, View
} from 'react-native';
import ProjectModal from './index'
import {PrimaryButton} from "../../../../components/Buttons/Primary";
import {Project} from "../../../../entities";
import {
    lastGeoPostionsSelector, locationParcelsSelector,
    locationPoisSelector, locationPolesSelector, locationSegmentsSelector,
    locationSelector,
    locationStationsSelector,
    moduleName
} from "../../../../ducks/map";
import {searchSelector} from "../../../../ducks/auth";
import {COLORS} from "../../../../styles/colors";
import styles from "../../../../styles/main";

interface MapProps {
    project: Project,
    showDialogContent: Function,
    projects: Array<any>
}

class ProjectListShow extends Component<MapProps> {

    private showProjects = () => {
        this.props.showDialogContent({
            content: <ProjectModal/>,
            header: <Text>Select Project</Text>
        })
    }

    render() {
        const {project} = this.props;
        return (
            <View style={styles.menuItem}>
                {
                    project ? (
                        <View style={localStyles.container}>
                            <Text style={styles.filterTitle}>
                                Project:
                            </Text>
                            <Text style={styles.filterActiveTitle}>{project.title}</Text>
                        </View>
                    ) : null
                }

                <PrimaryButton
                    style={{width: '100%'}}
                    title={'Select project'}
                    onPress={this.showProjects}
                />
            </View>
        );
    }
}


const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'center',
        flexDirection: 'row'
    }
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
const mapStateToProps = (state: any) => ({
    project: locationSelector(state)
});
export default connect(mapStateToProps, mapDispatchToProps)(ProjectListShow);


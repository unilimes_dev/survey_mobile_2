import React, {Component} from 'react';
import {Platform, FlatList, StyleSheet, ScrollView, Text, View, TouchableHighlight} from 'react-native';
import {moduleName, locationSelector, locationsSelector} from "../../../../ducks/map";
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

interface MapProps {
    showDialogContent: Function,
}


class MapFilter extends Component<MapProps> {


    render() {
        return (
            <View style={styles.container}>
                <Text>map Filters</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        width: '100%',
        minHeight: 120
    },
    scrollContent: {
        flex: 1,
        paddingTop: 5,
        width: '100%',
        minHeight: 120
    },
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
});

const mapStateToProps = (state: any) => ({
    error: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(MapFilter);


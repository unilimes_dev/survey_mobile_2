import React, {Component} from 'react';
import {showDialogContent} from "../../../../ducks/dialogs";

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    Platform,
    TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import MapFilter from './index'

interface MapProps {
    showDialogContent: Function,
    projects: Array<any>
}

class ProjectListShow extends Component<MapProps> {

    private showProjects = () => {
        this.props.showDialogContent(<MapFilter/>)
    }

    render() {
        return (
            <TouchableOpacity onPress={this.showProjects}>
                <Icon size={30}
                      style={{paddingRight: 20}}
                      name={Platform.OS === "ios" ? "ios-color-filter" : "md-color-filter"}
                />
            </TouchableOpacity>
        );
    }
}


const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
    }, dispatch)
);
export default connect(null, mapDispatchToProps)(ProjectListShow);


import React from "react";
import {View, Text, AsyncStorage, Dimensions, ScrollView} from "react-native";
import {
    createAppContainer,
    createStackNavigator,
    createDrawerNavigator,
    StackActions,
    NavigationActions
} from 'react-navigation'; // Version can be specified in package.json
import {DrawerItems, SafeAreaView} from 'react-navigation';
import DrawerScreen from './DrawerScreen';
import HomeScreen from './home';

class LogOut extends React.Component {
    async componentDidMount(): void {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    }

    render() {
        return null;
    }
}

const drawerConfig: any = {
    contentComponent: props => <DrawerScreen {...props} />,
    hideStatusBar: true,
    drawerBackgroundColor: 'rgba(255,255,255,.9)',
    overlayColor: '#6b52ae',
    initialRouteName: 'Home',
    drawerWidth: Dimensions.get("window").width,
    drawerPosition: 'right',
    contentOptions: {
        activeTintColor: '#fff',
        activeBackgroundColor: '#6b52ae',
    },
};
const DrawerNavigator = createDrawerNavigator(
    {
        Home: HomeScreen,
        LogOut: LogOut,
    },
    drawerConfig
);


export default createAppContainer(DrawerNavigator);

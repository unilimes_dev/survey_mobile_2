import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {
    ActivityIndicator,
    Dimensions,
    Platform,
    ScrollView,
    StyleSheet,
    Text, TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {DrawerActions} from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";
import {Image} from "react-native-elements";
import images from "../../../styles/images";
import styles from "../../../styles/main";
import {COLORS} from "../../../styles/colors";
import SvgUri from 'react-native-svg-uri';
import {
    lastGeoPostionsSelector, locationParcelsSelector,
    locationPoisSelector, locationPolesSelector, locationSegmentsSelector,
    locationSelector,
    locationStationsSelector,
    moduleName
} from "../../../ducks/map";

import {connect} from 'react-redux';
import {searchSelector} from "../../../ducks/auth";
import {bindActionCreators} from "redux";
import {changeContols} from "../../../ducks/map";

interface ItemProps {
    navigation: any,
    changeContols: Function,
    search: string
}

class Index extends Component<ItemProps> {


    private onChangeText = (value) => {
        this.props.changeContols({
            name: 'search',
            value
        });
    };
    private showMenu = () => {
        this.props.navigation.toggleDrawer();
    };

    render() {
        const isDrawerOpen = this.props.navigation.state.isDrawerOpen;
        if (isDrawerOpen) return null;
        return (
            <View style={localStyles.container}>
                <View style={localStyles.headerBar}>
                    <View style={localStyles.searchContainer}>
                        <SvgUri
                            width="30"
                            height="30"
                            source={images.SearchIcon}
                        />
                        <TextInput
                            value={this.props.search}
                            onChangeText={this.onChangeText}
                            placeholder={"Search your data"}
                            placeholderTextColor={COLORS.PRIMARY_1}
                            style={localStyles.searchInput}
                        />
                    </View>
                    <TouchableOpacity onPress={this.showMenu}>
                        <SvgUri
                            width="30"
                            height="30"
                            source={images.MenuIcon}
                        />
                    </TouchableOpacity>

                </View>

            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        padding: 10,
        paddingTop: 30,
        width: Dimensions.get("window").width,
        backgroundColor: 'red',
    },
    headerBar: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 10,
    },
    searchInput: {},
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }
});

const mapStateToProps = (state: any) => ({
    search: searchSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Index);

import {Map} from 'immutable';
import _ from 'lodash';

const _Map: any = Map;
export const arrayToMap = (arr: Array<any>, DataModule: any, key: string = 'ClinicalTrialID') => {
    return arr.reduce((
        acc: any, item: any) => acc.set(item[key], DataModule ? new DataModule(item) : item),
        new _Map({}),
    );
};
export const required = value => (value ? undefined : 'This is a required field.');
export const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(value) ? 'Please provide a valid email address.' : undefined;

export const checkError = (nextProps: any, curProps: any, onSuccess: Function, toast: any) => {

    if (!toast) return null;
    if (nextProps.error && !_.isEqual(nextProps.error, curProps.error)) {
        if (nextProps.error.error) {
            if (nextProps.error.error.original) {
                toast.show(nextProps.error.error.original.detail, {
                    position: toast.POSITION.TOP_LEFT
                });
            } else {
                if (typeof nextProps.error.error === "string") {
                    toast.show(nextProps.error.error, {
                        position: toast.POSITION.TOP_LEFT
                    });
                } else {
                    toast.show(nextProps.error.message, {
                        position: toast.POSITION.TOP_LEFT
                    });
                }
            }
        } else {
            toast.show(nextProps.error.error || nextProps.error.message, {
                position: toast.POSITION.TOP_LEFT
            });
        }
        onSuccess();
    }
}

export const moment = (date) => {
    const d1 = new Date(date);
    return {
        isSameOrBefore: (date: string) => {
            return d1.getTime() <= new Date(date).getTime()
        },
        isAfter: (date: Date) => {
            return d1.getTime() > new Date(date).getTime()
        }
    }
}
export const exportToCsv = function (filename: string, args: any) {
    let result: any, ctr: any, keys: any, columnDelimiter: any, lineDelimiter: any, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ';';
    lineDelimiter = args.lineDelimiter || '\n';

    const exclude = ['points'];
    keys = Object.keys(data[0]).filter((el) => exclude.indexOf(el) < 0);
    result = '';
    result += keys.join(columnDelimiter);
    result += columnDelimiter;

    data.forEach(function (item: any) {
        ctr = 0;
        keys.forEach(function (key: any) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];

            ctr++;
        });
        result += lineDelimiter;
    });

    var blob = new Blob([result], {type: 'text/csv;charset=utf-8;'});
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
    return result;
}

export const _exportToCsv = function (filename: string, rows: Array<any>) {
    console.log(JSON.stringify(rows));
    var processRow = function (row: any) {
        var finalVal = '';
        const delimeter = ";";
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            }
            var result = innerValue.replace(/"/g, '""');
            // if (result.search(/("|,|\n)/g) >= 0)
            //     result = '"' + result + '"';
            if (j > 0)
                finalVal += delimeter;
            finalVal += result;
        }
        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], {type: 'text/csv;charset=utf-8;'});
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
export const statuses = [
    {
        value: 1,
        text: 'Undefined'
    },
    {
        value: 2,
        text: 'Permission grandted'
    },
    {
        value: 3,
        text: 'no permission'
    },
];


export const segment_statuses = [
    {
        value: 2,
        text: 'Empty'
    },
    {
        value: 1,
        text: 'Vegetated'
    },
    {
        value: 3,
        text: 'Urgent'
    },
    {
        value: 4,
        text: 'Shutoff'
    },
    {
        value: 5,
        text: 'Service'
    },
    {
        value: 6,
        text: 'Unverified'
    },
    {
        value: 7,
        text: 'No Permission'
    },
];

export const segment_operation_type = [
    {
        value: 'mulcher giraffe',
        text: 'mulcher giraffe'
    },
    {
        value: 'chainsaw',
        text: 'chainsaw'
    },
    {
        value: 'arborists',
        text: 'arborists'
    }
];
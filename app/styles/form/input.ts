import {StyleSheet} from 'react-native'
import {COLORS} from '../colors';

const _styles: any = {
    input: {
        borderBottomColor: `${COLORS.PRIMARY}`,
        borderBottomWidth: 1,
        backgroundColor: 'white',
        padding: 18,
        marginBottom: 10,
        fontSize: 16,
        width: `100%`
    },
    placeholderStyle: {}
}
const styles = StyleSheet.create(_styles);

export default styles

import {StyleSheet} from 'react-native'
import {COLORS} from '../colors';

const _styles: any = {
    primary: {
        backgroundColor: COLORS.PRIMARY,
        width: `100%`
    }
}
const styles = StyleSheet.create(_styles);

export default styles

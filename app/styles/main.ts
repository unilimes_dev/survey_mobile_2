import {StyleSheet, Dimensions} from 'react-native'
import {COLORS} from './colors';

const _styles: any = {
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    title: {
        textAlign: 'center',
        padding: 5,
        fontSize: 24,
        color: COLORS.PRIMARY_1,
        opacity: 0.7
    },
    filterActiveTitle: {
        textAlign: 'center',
        padding: 5,
        fontSize: 14,
        color: COLORS.PRIMARY_1,
    },
    filterTitle: {
        textAlign: 'center',
        padding: 5,
        fontSize: 14,
        color: COLORS.PRIMARY_1,
        opacity: 0.7
    },

    logoImg: {
        // flex: 1,
        // justifyContent: 'center',
        marginTop: 20,
        width: '100%',
    },
    logoImgContainer: {
        padding: 10,
        width: Dimensions.get('window').width - 20,
    },
    mainPage: {
        flex: 1,
        justifyContent: 'center'
    },
    menuItem: {
        borderBottomColor: COLORS.GRAY,
        borderBottomWidth: 1,
        paddingBottom: 10,
        paddingTop: 10,
    },
};

const _list_styles: any = {
    item: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 0.5,
    },
    itemSelected: {
        width: '100%',
        padding: 10,
        fontSize: 14,
        height: 44,
        opacity: 1,
    },
};
const styles = StyleSheet.create(_styles);
export const list_styles = StyleSheet.create(_list_styles);

export default styles

import LOGO from '../../assets/img/logo.png';
import PoiIcon from '../../assets/img/poi.png';
import StationIcon from '../../assets/img/station.png';
import PoleIcon from '../../assets/img/Pole.png';
import TableViewIcon from '../../assets/img/tableview-active.svg';
import MapViewIcon from '../../assets/img/mapview-active.svg';
import SyncIcon from '../../assets/img/sync.svg';
import ExportIcon from '../../assets/img/export.svg';
import SearchIcon from '../../assets/img/search.svg';
import MenuIcon from '../../assets/img/menu.svg';
import SegmentIcon from '../../assets/img/new/Segment.svg';
import ParcelIcon from '../../assets/img/new/parcel.svg';

const images = {
    ParcelIcon,
    SegmentIcon,
    MenuIcon,
    SearchIcon,
    SyncIcon,
    ExportIcon,
    MapViewIcon,
    TableViewIcon,
    PoleIcon,
    StationIcon,
    LOGO,
    PoiIcon

};


export default images;

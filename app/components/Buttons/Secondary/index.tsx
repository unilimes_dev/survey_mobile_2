import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import {COLORS} from '../../../styles/colors';
import {CirclesLoader} from 'react-native-indicator';

/*interface ItemProps {
    disabled: boolean,
    onPress: any,
    textStyle: any,
    style: any,
    title: any
}*/

export const SecondaryButton = (props: any) => {
    const {
        title = 'Enter',
        style = {},
        textStyle = {},
        disabled = false,
        onPress
    } = props;

    const _onPress = (e) => {
        if (disabled) {

        } else {
            onPress(e);
        }
    }
    return (
        <TouchableOpacity onPress={_onPress} style={[styles.button, style, disabled ? styles.disabled : null]}>
            <Text style={[styles.text, textStyle]}>{title}</Text>
            {
                disabled ? <CirclesLoader/> : null
            }
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',

        color: COLORS.PRIMARY,
        opacity: 0.7,
        shadowOpacity: 0.4,
        shadowOffset: {height: 10, width: 0},
        shadowRadius: 20,
    },
    disabled: {
        opacity: 0.7
    },
    text: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: COLORS.PRIMARY,
    },
});

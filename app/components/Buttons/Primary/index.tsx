import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import {COLORS} from '../../../styles/colors';
import {CirclesLoader} from 'react-native-indicator';

/*interface ItemProps {
    disabled: boolean,
    onPress: any,
    textStyle: any,
    style: any,
    title: any
}*/

export const PrimaryButton = (props: any) => {
    const {
        title = 'Enter',
        style = {},
        textStyle = {},
        disabled = false,
        variant = 'primary',
        onPress
    } = props;

    const _onPress = (e) => {
        if (disabled) {

        } else {
            onPress(e);
        }
    }
    return (
        <TouchableOpacity onPress={_onPress}
                          style={
                              [
                                  variant === 'secondary' ? styles.button_scn : styles.button,
                                  style,
                                  disabled ? styles.disabled : null
                              ]
                          }>
            <Text style={[variant === 'secondary' ? styles.text_scn : styles.text, textStyle]}>{title}</Text>
            {
                disabled ? <CirclesLoader/> : null
            }

        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        height: 50,
        minWidth: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

        backgroundColor: COLORS.PRIMARY,
        shadowColor: COLORS.PRIMARY,
        shadowOpacity: 0.4,
        shadowOffset: {height: 10, width: 0},
        shadowRadius: 20,
    },
    button_scn: {
        display: 'flex',
        height: 50,
        minWidth: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

        backgroundColor: 'white',
        shadowColor: 'white',
        shadowOpacity: 0.0,
        shadowOffset: {height: 10, width: 0},
        shadowRadius: 20,
    },
    disabled: {
        opacity: 0.7
    },

    text: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: '#FFFFFF',
    },
    text_scn: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: COLORS.PRIMARY,
    },
});

import React from 'react';
import {TouchableOpacity, Platform, StyleSheet, Text} from 'react-native';
import {COLORS} from '../../../styles/colors';
import {CirclesLoader} from 'react-native-indicator';
import Icon from "react-native-vector-icons/Ionicons";

/*interface ItemProps {
    disabled: boolean,
    onPress: any,
    textStyle: any,
    style: any,
    title: any
}*/

export const FabButton = (props: any) => {
    const {
        title = 'Enter',
        style = {},
        textStyle = {},
        disabled = false,
        onPress
    } = props;

    const _onPress = (e) => {
        if (disabled) {

        } else {
            onPress(e);
        }
    }
    return (
        <TouchableOpacity onPress={_onPress} style={[styles.button, style, disabled ? styles.disabled : null]}>
            <Icon size={30}
                  style={styles.icon}
                  name={Platform.OS === "ios" ? "ios-add" : "md-add"}
            />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

        borderRadius: 100,
        backgroundColor: COLORS.PRIMARY,
        shadowColor: COLORS.PRIMARY,
        shadowOpacity: 0.4,
        shadowOffset: {height: 10, width: 0},
        shadowRadius: 20,
    },
    disabled: {
        opacity: 0.7
    },

    icon: {
        padding: 20,
        fontSize: 16,
        textTransform: 'uppercase',
        color: '#FFFFFF',
    },
});

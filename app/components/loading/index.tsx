import {CirclesLoader, PulseLoader, TextLoader, DotsLoader} from 'react-native-indicator';
import React, {Component} from 'react';
import {View} from 'react-native';

export default class Loading extends Component {
    render() {
        return (
            <View>
                <CirclesLoader/>
                <TextLoader text="Loading"/>
            </View>
        );
    }
}

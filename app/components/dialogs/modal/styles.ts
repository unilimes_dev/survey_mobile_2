import {StyleSheet, Dimensions} from "react-native";

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        height: '70%'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconClose: {
        position: 'absolute',
        right: 0,
        top: 20
    },
    modalTitle: {
        // position: 'absolute',
        top: 0,
        width: '100%',
        // flex: 1,
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    modalHeader: {
        // flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#315581',
        padding: 20,
        opacity: 0.5
    },
    button: {
        bottom: 0,
        padding: 10,
        // position: 'absolute',
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent:'flex-end',
        backgroundColor: 'white'
    },
    modalEl: {
        position: 'relative',
        width: Dimensions.get('window').width * 0.9,
        overflow: 'scroll',
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent: {
        position: 'relative',
        width: '100%',
        height: Dimensions.get('window').height * 0.7,
        overflow: 'scroll',
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
});
export default styles

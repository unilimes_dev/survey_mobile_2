import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons'

import styles from './styles'

import {TouchableOpacity, Text} from 'react-native'

const CheckBox = ({selected = false, onPress = (): any => 1, style=null, textStyle=null, size = 30, color = '#211f30', text = null, ...props}) => (
    <TouchableOpacity style={[styles.checkBox, style]} onPress={onPress} {...props}>
        <Icon
            size={size}
            color={color}
            name={selected ? 'check-box' : 'check-box-outline-blank'}
        />

        {text}
        {/*<Text style={textStyle}> {text} </Text>*/}
    </TouchableOpacity>
)

export default CheckBox
